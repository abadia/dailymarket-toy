## dailymarket-toy

Calculate demand profile for spanish daily market operations based on REE specifications [^ref]

Demand and profile coefficients used are based on REE coefficients [^coef]

[^ref]: BOE Núm: 312 Sec:1 Resolución de 23 de diciembre de 2015, de la Dirección General de Política Energética y Minas
[^coef]: REE Demanda de referencia y perfiles iniciales 2016
