from datetime import date
import numpy as np

PERIODS = ['P1','P2','P3']
PERFS = ['pa', 'pb', 'pc', 'pd']
PERF_TO_TARIFF = {
    'pa': ['2.0A'  , '2.1A'],
    'pb': ['2.0DHA', '2.1DHA'],
    'pc': ['3.0A'  , '3.1A'],
    'pd': ['2.0DHS', '2.1DHS']
    }

def tokwh(mwh):
    return mwh*1000

def tomwh(kwh):
    return int(np.round((kwh/1000.0)))


class Aggregator(object):
    def __init__(self, spec):
        if 'contracts' not in spec:
            assert('Missing contract specification')
        if 'anual' not in spec:
            assert('Missing contract specification')
        self.spec = spec

    def get_by_tariff(self, tariff):
        if tariff not in self.spec:
            assert('Missing %s tariff in specification' % tariff)

        anual = np.array([
            tomwh(self.spec.contracts[tariff]*self.spec.anual[tariff][period])
            for period in PERIODS])
        return anual

    def get_by_perf(self, perf):
        if perf not in PERFS:
            assert('Missing %s perf' % perf)

        return np.sum(self.get_by_tariff(tariff)
                for tariff in PERF_TO_TARIFF[perf])

    def apply_leaks(self, perf, aggr):
        if perf not in PERFS:
            assert('Missing %s perf' % perf)

        return aggr*np.array([
            float(self.spec.leaks[perf][period]) for period in PERIODS])

    def apply_risk(self, perf, aggr):
        if perf not in PERFS:
            assert('Missing %s perf' % perf)

        return aggr*np.array([
            float(self.spec.risk[perf][period]) for period in PERIODS])

    def get_by_perff(self, perf):
        return np.round(self.apply_risk(perf,self.apply_leaks(
            perf,self.get_by_perf(perf))))

    def get_refdaily(self):
        return float(self.spec.refdaily)

    def get_refdate(self):
        return self.spec.refdate

class Profiler(object):
    dt = ['year','month','day','hour']

    def __init__(self, perfi, aggr, year):
        self.aggr = aggr

        names = ['year','month','day','hour','dst',
                'coefa','coefb','coefc','coefd',
                'perb','perc','perd'] 
        self.data = np.genfromtxt(perfi,
                skip_header=1, 
                delimiter=';',
                names=names)
 
    def get_refdaily(self, dt):
        refdate = self.aggr.get_refdate()
        refdaily = self.aggr.get_refdaily()
        return np.array([
            (date(int(d[0]), int(d[1]), int(d[2]))-refdate).days*refdaily
            for d in dt
            ])

    def get_profile_pa(self):
        pa_mwh = self.aggr.get_by_perff('pa')[0]
        dt = self.data[self.dt]
        return (self.data[self.dt],
                (self.data['coefa']/np.sum(
                    self.data['coefa']))*(pa_mwh+self.get_refdaily(dt)))

    def get_profile_px(self, coef, per, pids, mwh):
        p = np.zeros(self.data.shape[0])
        dt = self.data[self.dt]
        for pid in pids:
            mask = self.data[per]==pid
            p[mask] = (self.data[coef][mask]/np.sum(
                self.data[coef][mask]))*mwh[pid-1]
        return (dt, p)

    def get_profile_pb(self):
        pb_mwh = self.aggr.get_by_perff('pb')
        return self.get_profile_px('coefb', 'perb', [1,2], pb_mwh)

    def get_profile_pc(self):
        pc_mwh = self.aggr.get_by_perff('pc')
        return self.get_profile_px('coefc', 'perc', [1,2,3], pc_mwh)

    def get_profile_pd(self):
        pd_mwh = self.aggr.get_by_perff('pd')
        return self.get_profile_px('coefd', 'perd', [1,2,3], pd_mwh)

    def get_profile(self):
        dt,pa = self.get_profile_pa() 
        _,pb = self.get_profile_pb() 
        _,pc = self.get_profile_pc() 
        _,pd = self.get_profile_pd()
        return (dt, pa+pb+pc+pd)

#vim: ts=4 sw=4 et
