import os
from datetime import datetime,date,timedelta

class Writer(object):
    year=0
    month=0
    day=0
    def __init__(self):
        pass

    def is_new_day(self, dt):
        return not(dt[0]==self.year and
                dt[1]==self.month and
                dt[2]==self.day)

    def update_day(self, dt):
        self.year = dt[0]
        self.month = dt[1]
        self.day = dt[2]

    def rightpad(self, dp):
        return dp + [0] * (3+24-len(dp))

    def write(self, p, filename):
        pass

class PenWriter(Writer):
    def write(self, dt, p, filename):
        import csv
        with open(filename, 'w') as f:
            writer = csv.writer(f, delimiter=';', lineterminator='\n')
            day_profile = []
            for idx,d in enumerate(dt):
                if self.is_new_day(d):
                    if idx:
                        writer.writerow(self.rightpad(day_profile))
                    self.update_day(d)
                    day_profile = [d[0], d[1], d[2]]
                day_profile.append('%0.2f' % p[idx])
            writer.writerow(self.rightpad(day_profile))

class IcWriter(Writer):
    def write(self, dt, p, filename):
        import csv
        with open(filename, 'w') as f:
            writer = csv.writer(f, delimiter=';', lineterminator='\n')
            day_profile = []
            for idx,d in enumerate(dt):
                if self.is_new_day(d):
                    if idx:
                        writer.writerow(self.rightpad(day_profile))
                    self.update_day(d)
                    day_profile = ['XXXXTF','%02d/%02d/%04d' % (d[2], d[1], d[0])]
                day_profile.append('%0.2f' % p[idx])
            writer.writerow(self.rightpad(day_profile))

class IbWriter(Writer):
    def isoformat(self, dt, seconds=False):
        return dt.strftime('%Y-%m-%dT%H:%M:%SZ') if seconds else dt.strftime('%Y-%m-%dT%H:%MZ')

    def write(self, dt, p, filename, cdt=None):
        import lxml.etree
        import lxml.builder
        from pytz import timezone as timezone_, utc as utc_

        E = lxml.builder.ElementMaker()

        dt0 = dt[0]
        tz = timezone_('Europe/Madrid')
        start = tz.localize(
                datetime(dt0[0],dt0[1],dt0[2],0,0,0)).astimezone(utc_)
        cdt = tz.localize(cdt).astimezone(utc_)
        end = start + timedelta(days=7)
        identificationMensaje_ = 'DemPrevDiario_XXXX_%04d%02d%02d' % (cdt.year, cdt.month, cdt.day)
        versionMensaje_ = '1'

        periodos = []
        intervalos = []
        h = 1
        o = dt[0]
        for idx,d in enumerate(dt):
            if self.is_new_day(d):
                if idx:
                    h = 1
                    sofday = tz.localize(
                            datetime(o[0], o[1], o[2], 0, 0, 0)).astimezone(utc_)
                    eofday = sofday + timedelta(days=1)
                    periodos.append(E.Periodo(
                            E.IntervaloTiempo(v='%s/%s' % (
                                self.isoformat(sofday),
                                self.isoformat(eofday))),
                            E.Resolucion(v="PT60M"), *intervalos))
                    o = d
                    intervalos = []
                self.update_day(d)
            intervalos.append(
                    E.Intervalo(E.Pos(v=' %d' % h),E.Ctd(v='0%0.3f' % p[idx])))
            h += 1
        sofday = tz.localize(
                datetime(d[0], d[1], d[2], 0, 0, 0)).astimezone(utc_)
        eofday = sofday + timedelta(days=1)
        periodos.append(E.Periodo(
                E.IntervaloTiempo(v='%s/%s' % (
                    self.isoformat(sofday),
                    self.isoformat(eofday))),
                E.Resolucion(v="PT60M"), *intervalos))

        doc = E.Demanda(
                E.IdentificacionMensaje(v=identificationMensaje_),
                E.VersionMensaje(v="64"),
                E.TipoMensaje(v="Z03"),
                E.TipoProceso(v="Z05"),
                E.SEIE(v="18YBALEARES----9",codificacion="A01"),
                E.IdentificacionRemitente(v="XXXXXX-XXXX-U",codificacion="A01"),
                E.FuncionRemitente(v="A08"),
                E.IdentificacionDestinatario(v="10XES-REE------E",codificacion="A01"),
                E.FuncionDestinatario(v="A04"),
                E.FechaHoraMensaje(v=self.isoformat(cdt,seconds=True)),
                E.Horizonte(v="%s/%s" % (self.isoformat(start), self.isoformat(end))),
                E.SeriesTemporales(
                    E.IdentificacionSeriesTemporales(v="IST01"),
                    E.UP(codificacion="A01",v="XXXXXX-XXX-E"),
                    E.UnidadMedida(v="MWH"),
                    *periodos
                ),
                xmlns="http://esole.ree.es/schemas/2009/01/01/Demanda-esole-ASE/"
        )
        with open(filename, 'w') as f:
            f.write(lxml.etree.tostring(doc,
                pretty_print=True, xml_declaration=True, encoding='utf-8'))

# vim: ts=4 sw=4 et
