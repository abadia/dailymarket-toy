from datetime import date
import unittest
import numpy as np
from yamlns import namespace as ns

from dailymarket import Aggregator, Profiler

def toa(a):
    return a.tolist()

class Aggregator_Test(unittest.TestCase):
    def test_20A(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_tariff('2.0A')
        self.assertEqual(toa(anual_mwh), [100, 0, 0])

    def test_20DHA(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_tariff('2.0DHA')
        self.assertEqual(toa(anual_mwh), [100, 100, 0])

    def test_20DHS(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_tariff('2.0DHS')
        self.assertEqual(toa(anual_mwh), [100, 100, 100])

    def test_21A(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_tariff('2.1A')
        self.assertEqual(toa(anual_mwh), [100, 0, 0])

    def test_21DHA(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_tariff('2.1DHA')
        self.assertEqual(toa(anual_mwh), [100, 100, 0])

    def test_21DHS(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_tariff('2.1DHS')
        self.assertEqual(toa(anual_mwh), [100, 100, 100])

    def test_30A(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_tariff('3.0A')
        self.assertEqual(toa(anual_mwh), [100, 100, 100])

    def test_31A(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_tariff('3.1A')
        self.assertEqual(toa(anual_mwh), [100, 100, 100])

    def test_pa(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_perf('pa')
        self.assertEqual(toa(anual_mwh), [100, 100, 100])

    def test_pb(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_perf('pb')
        self.assertEqual(toa(anual_mwh), [100, 100, 0])

    def test_pc(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_perf('pc')
        self.assertEqual(toa(anual_mwh), [100, 100, 100])

    def test_pd(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        anual_mwh = aggr.get_by_perf('pd')
        self.assertEqual(toa(anual_mwh), [100, 100, 100])

    def test_apply_leaks_pa(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        aggr_ = np.array([1, 1, 1])
        self.assertEqual(toa(aggr.apply_leaks('pa', aggr_)), [100, 0, 0])

    def test_apply_leaks_pb(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        aggr_ = np.array([1, 1, 1])
        self.assertEqual(toa(aggr.apply_leaks('pb', aggr_)), [100, 100, 0])

    def test_apply_leaks_pc(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        aggr_ = np.array([1, 1, 1])
        self.assertEqual(toa(aggr.apply_leaks('pc', aggr_)), [100, 100, 100])

    def test_apply_leaks_pd(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        aggr_ = np.array([1, 1, 1])
        self.assertEqual(toa(aggr.apply_leaks('pd', aggr_)), [100, 100, 100])

    def test_apply_risk_pa(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        aggr_ = np.array([1, 1, 1])
        self.assertEqual(toa(aggr.apply_risk('pa', aggr_)), [100, 100, 100])

    def test_apply_risk_pb(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        aggr_ = np.array([1, 1, 1])
        self.assertEqual(toa(aggr.apply_risk('pb', aggr_)), [100, 100, 100])

    def test_apply_risk_pc(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        aggr_ = np.array([1, 1, 1])
        self.assertEqual(toa(aggr.apply_risk('pc', aggr_)), [100, 100, 100])

    def test_apply_risk_pd(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        aggr_ = np.array([1, 1, 1])
        self.assertEqual(toa(aggr.apply_risk('pd', aggr_)), [100, 100, 100])

    def test_get_perff_pa(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        self.assertEqual(toa(aggr.get_by_perff('pa')), [100, 100, 100])

    def test_get_perff_pb(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        self.assertEqual(toa(aggr.get_by_perff('pb')), [100, 100, 100])

    def test_get_perff_pc(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        self.assertEqual(toa(aggr.get_by_perff('pc')), [100, 100, 100])

    def test_get_perff_pd(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        self.assertEqual(toa(aggr.get_by_perff('pd')), [100, 100, 100])

    def test_get_refdaily(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        self.assertEqual(aggr.get_refdaily(), 100) 

    def test_get_refdate(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        self.assertEqual(aggr.get_refdate(), date(2016,9,21))

class Profiler_Test(unittest.TestCase):
    def setupProfileTest(self):
        spec_path = 'b2bdata/dailymarket.yaml'
        perfi_path = 'dailymarket/data/perfi_2016_v1_1.csv'
        spec = ns.load(spec_path)
        aggr = Aggregator(spec)
        return Profiler(perfi_path, aggr, 2016)

    def assertEqualProfile(self, ref, p):
        with open(ref) as f:
            refp = [float(l.rstrip()) for l in f.readlines()]
        for i in range(len(refp)):
            self.assertAlmostEqual(refp[i], p[i], places=2)

    def assertEqualDates(self, ref, d):
        with open(ref) as f:
            refp = [(l.rstrip().split(';')) for l in f.readlines()]
        for i in range(len(refp)):
            self.assertEqual(float(refp[i][0]), d[i][0])
            self.assertEqual(float(refp[i][1]), d[i][1])
            self.assertEqual(float(refp[i][2]), d[i][2])

    def test_get_profile_a(self):
        profiler = self.setupProfileTest()
        dates,pa = profiler.get_profile_pa()
        self.assertEqualProfile('b2bdata/test_pa_2016.csv', pa)

    def test_get_profile_b(self):
        profiler = self.setupProfileTest()
        dates,pb = profiler.get_profile_pb()
        self.assertEqualProfile('b2bdata/test_pb_2016.csv', pb)

    def test_get_profile_c(self):
        profiler = self.setupProfileTest()
        dates,pc = profiler.get_profile_pc()
        self.assertEqualProfile('b2bdata/test_pc_2016.csv', pc)

    def test_get_profile_d(self):
        profiler = self.setupProfileTest()
        dates,pd = profiler.get_profile_pd()
        self.assertEqualProfile('b2bdata/test_pd_2016.csv', pd)

    def test_get_profile(self):
        profiler = self.setupProfileTest()
        dates,p = profiler.get_profile()
        self.assertEqualProfile('b2bdata/test_p_2016.csv', p)

    def test_profile_dates(self):
        profiler = self.setupProfileTest()
        dt,p = profiler.get_profile()
        self.assertEqualDates('b2bdata/test_dates_2016.csv', dt)

# vim: ts=4 sw=4 et
