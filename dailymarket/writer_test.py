from datetime import date,datetime
import tempfile
import filecmp

import unittest
from xmlunittest import XmlTestCase

from writer import PenWriter, IbWriter, IcWriter

class Writer_Test(unittest.TestCase):
    def assertFilesEqual(self, f1, f2):
        return self.assertTrue(filecmp.cmp(f1,f2))

    def test_penwriter_write_oneday(self):

        dt = [(2016,1,1,h) for h in range(1,24)]
        p = [h*0.1 for h in range(1,24)]

        with tempfile.NamedTemporaryFile(delete=True) as f:
            pen = PenWriter()
            pen.write(dt, p, f.name)
            self.assertFilesEqual(
                    './b2bdata/test_penwriter_writer_oneday.csv', f.name)

    def test_penwriter_write_twoday(self):

        dt =  [(2016,1,1,h) for h in range(1,24)]
        dt += [(2016,1,2,h) for h in range(1,24)]
        p = 2*[h*0.1 for h in range(1,24)]

        with tempfile.NamedTemporaryFile(delete=True) as f:
            pen = PenWriter()
            pen.write(dt, p, f.name)
            self.assertFilesEqual(
                    './b2bdata/test_penwriter_writer_twoday.csv', f.name)

    def test_penwriter_write_threeday(self):

        dt =  [(2016,1,1,h) for h in range(1,24)]
        dt += [(2016,1,2,h) for h in range(1,24)]
        dt += [(2016,1,3,h) for h in range(1,24)]
        p = 2*[h*0.1 for h in range(1,24)]+24*[0]

        with tempfile.NamedTemporaryFile(delete=True) as f:
            pen = PenWriter()
            pen.write(dt, p, f.name)
            self.assertFilesEqual(
                    './b2bdata/test_penwriter_writer_threeday.csv', f.name)

    def test_icwriter_write_oneday(self):

        dt = [(2016,1,1,h) for h in range(1,24)]
        p = [h*0.1 for h in range(1,24)]

        with tempfile.NamedTemporaryFile(delete=True) as f:
            ic = IcWriter()
            ic.write(dt, p, f.name)
            self.assertFilesEqual(
                    './b2bdata/test_icwriter_writer_oneday.csv', f.name)

    def test_icwriter_write_twoday(self):

        dt =  [(2016,1,1,h) for h in range(1,24)]
        dt += [(2016,1,2,h) for h in range(1,24)]
        p = 2*[h*0.1 for h in range(1,24)]

        with tempfile.NamedTemporaryFile(delete=True) as f:
            ic = IcWriter()
            ic.write(dt, p, f.name)
            self.assertFilesEqual(
                    './b2bdata/test_icwriter_writer_twoday.csv', f.name)

    def test_icwriter_write_threeday(self):

        dt =  [(2016,1,1,h) for h in range(1,24)]
        dt += [(2016,1,2,h) for h in range(1,24)]
        dt += [(2016,1,3,h) for h in range(1,24)]
        p = 2*[h*0.1 for h in range(1,24)]+24*[0]

        with tempfile.NamedTemporaryFile(delete=True) as f:
            ic = IcWriter()
            ic.write(dt, p, f.name)
            self.assertFilesEqual(
                    './b2bdata/test_icwriter_writer_threeday.csv', f.name)

# vim: ts=4 sw=4 et
